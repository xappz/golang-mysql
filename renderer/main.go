package renderer
import (
	"log"
	"net/http"
	"bitbucket.org/xappz/golang-mysql/deps"
	"html/template"
	"github.com/gorilla/mux"
)

type renderer struct {
	logger *log.Logger
	sm deps.SessionMgr
	templates map[string]*template.Template
}

func (c renderer) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	switch (vars["page"]) { 
	case "users":
		handleUsers(c, w, r)
		break

	default:
		c.templates["404"].Execute(w, nil)
	}
}

type temp struct {
	Name string
}

func handleUsers(c renderer, w http.ResponseWriter, r *http.Request) {
	name := c.sm.Get(r, "name")
	name += "a"

	c.sm.Set(r, "name", name)
	c.sm.Save(w, r)
	c.templates["users"].Execute(w, temp{Name: name})
}

//this is just a shortcut method to create renderer if you have a container available
func Create(c deps.Container) http.Handler {
	return renderer{logger: c.Logger("renderer"), templates: c.Templates, sm: c.Sm}
}

func New(logger *log.Logger, templates map[string]*template.Template, sm deps.SessionMgr) http.Handler {
	return renderer{logger: logger, templates: templates, sm: sm}
}
