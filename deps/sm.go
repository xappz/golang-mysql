package deps
import (
	"net/http"
	"github.com/gorilla/sessions"
)

type SessionMgr interface {
	Set(*http.Request, string, string)
	Get(*http.Request, string) string
	Save(http.ResponseWriter, *http.Request)
}

type FactoryFunc func() SessionMgr 

type mgr struct {
	store *sessions.CookieStore
}

func (sm mgr) Get(r *http.Request, key string) string {
	session, _ := sm.store.Get(r, "session-name")
	value := session.Values[key]
	if value == nil {
		return ""
	}

	return value.(string)
}

func CreateSm(key string) SessionMgr {
	return mgr{store: sessions.NewCookieStore([]byte(key))}
}

func (sm mgr) Set(r *http.Request, key string, value string) {
	session, _ := sm.store.Get(r, "session-name")
	session.Values[key] = value
}

func (sm mgr) Save(w http.ResponseWriter, r *http.Request) {
	session, _ := sm.store.Get(r, "session-name")
	session.Save(r, w)
}
