package deps
import (
	"log"
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
	"github.com/spf13/viper"
	"html/template"
	"github.com/eknkc/amber"
	"os"
	"github.com/gorilla/sessions"
)

type LoggerFunc func(string) *log.Logger

type Container struct {
	//Store *sessions.CookieStore
	Db *sql.DB
	Templates map[string]*template.Template
	Logger LoggerFunc
	Sm SessionMgr
}

func Create() Container {
	return Container{
		//Store : CreateStore(viper.GetString("store-key")),
		Db : CreateDb(viper.GetString("user"), viper.GetString("password"), viper.GetString("db")),
		Templates: CreateTemplates(viper.GetString("templates")),
		Logger: CreateLogger(viper.GetString("logs")),
		Sm: CreateSm(viper.GetString("store-key"))}
}

func CreateStore(key string) *sessions.CookieStore {
	 return sessions.NewCookieStore([]byte(key))
}

func CreateLogger(path string) LoggerFunc {
	return func(name string) *log.Logger {
		out, err := os.OpenFile(path + "/" + name + ".log",
			os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
		if nil != err {
			panic(err.Error())
		}

		logger := log.New(out, "", log.Lshortfile | log.Ldate | log.Ltime)
		return logger;
	}
}

func CreateDb(user string, password string, dbName string) *sql.DB {
	connStr := user + ":" + password + "@/" + dbName
	db, err := sql.Open("mysql", connStr)
	if err != nil {
		panic(err.Error())
	}

	err = db.Ping()
	if err != nil {
		panic(err.Error())
	}

	return db;
}

func CreateTemplates(path string) map[string]*template.Template {
	templates, err := amber.CompileDir(path, amber.DefaultDirOptions, amber.DefaultOptions)
	if err != nil {
		panic(err.Error())
	}
	return templates
}

func (c Container) Destroy() {
	c.Db.Close()
}
