package tests
import (
	"net/http"
	"net/http/httptest"
	"testing"
	"bitbucket.org/xappz/golang-mysql/deps"
	"bitbucket.org/xappz/golang-mysql/controllers"
	"bitbucket.org/xappz/golang-mysql/models"
)

func TestUsrCtrlr(t *testing.T) {
    req, err := http.NewRequest("GET", "/api/users/5", nil)
    if err != nil {
        t.Fatal(err)
    }

    loggerProvider := deps.CreateLogger("./logs")
    logger := loggerProvider("test")
    db := deps.CreateDb("server", "Kingfisher#444", "dev-generico")
    model := models.NewUser(db, logger)

    rr := httptest.NewRecorder()
    handler := controllers.UsrCtrlr(logger, model) 
    handler.ServeHTTP(rr, req)

    // Check the status code is what we expect.
    if status := rr.Code; status != http.StatusOK {
        t.Errorf("handler returned wrong status code: got %v want %v",
            status, http.StatusOK)
    }
}
