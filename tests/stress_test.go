package tests
import (
	//"net/http"
	"testing"
	"log"
	"time"
	"math/rand"
	"net/http"
	"sync"
	"io/ioutil"
	"strconv"
)

func TestStress(t *testing.T) {
	var wg sync.WaitGroup
	for i := 0; i < 100; i++ {
		// Increment the WaitGroup counter.
		wg.Add(1)
		// Launch a goroutine to fetch the URL.
		go func(i int) {
			// Decrement the counter when the goroutine completes.
			defer wg.Done()

			// Fetch the URL after sleeping for random time
			time.Sleep(time.Duration(rand.Intn(5)) * time.Millisecond)
			id := rand.Intn(10)
			res, err := http.Get("http://localhost:3000/api/users/" + strconv.Itoa(id))
			if err != nil {
				t.Errorf(err.Error())
				return
			}

			defer res.Body.Close()
			body, _ := ioutil.ReadAll(res.Body)
			log.Println(strconv.Itoa(id) + ":" + string(body))
		}(i)
	}
	// Wait for all HTTP fetches to complete.
	wg.Wait()
}
