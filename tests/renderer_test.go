package tests
import (
	"net/http"
	"net/http/httptest"
	"testing"
	"bitbucket.org/xappz/golang-mysql/deps"
	"bitbucket.org/xappz/golang-mysql/renderer"
)

func TestRenderer(t *testing.T) {
    req, err := http.NewRequest("GET", "/users", nil)
    if err != nil {
        t.Fatal(err)
    }

    loggerProvider := deps.CreateLogger("./logs")
    logger := loggerProvider("test")

    rr := httptest.NewRecorder()
    handler := renderer.New(logger, deps.CreateTemplates("./templates"), deps.CreateSm("super-secret-key"))
    handler.ServeHTTP(rr, req)

    // Check the status code is what we expect.
    if status := rr.Code; status != http.StatusOK {
        t.Errorf("handler returned wrong status code: got %v want %v",
            status, http.StatusOK)
    }
}
