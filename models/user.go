package models
import (
	"log"
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
	"bitbucket.org/xappz/golang-mysql/common"
	"net/http"
	"github.com/asaskevich/govalidator"
)

type User struct {
	Id int `json:"id"`
	Name string `json:"name"`
}

type Usr interface {
	FindByName(string) ([]*User, *common.AppError)
	FindById(int) (*User, *common.AppError)
	Save(string, string) (int64, *common.AppError)
}

type user struct {
	//private stuff
	db *sql.DB
	logger *log.Logger
}

func NewUser(db *sql.DB, logger *log.Logger) *user {
	return &user{db, logger}
}

//http://go-database-sql.org/retrieving.html
func (u *user) FindById(id int) (*User, *common.AppError) {
	rows, err := u.db.Query("select id, name from users where id = ?", id)
	if err != nil {
		u.logger.Println(err.Error())
		return nil, &common.AppError{err, "DB Server error", http.StatusInternalServerError}
	}

	defer rows.Close()

	user := new(User)

	for rows.Next() {
		err := rows.Scan(&user.Id, &user.Name)
		if err != nil {
			u.logger.Println(err.Error())
			return nil, &common.AppError{err, "DB Server error", http.StatusInternalServerError}
		}
	}

	err = rows.Err()
	if err != nil {
		u.logger.Println(err.Error())
		return nil, &common.AppError{err, "DB Server error", http.StatusInternalServerError}
	}

	return user, nil
}

func (u *user) Save(name string, email string) (int64, *common.AppError) {
	//todo: should use stringlength here
	if (!govalidator.IsByteLength(name, 2, 100)) {
		return 0, &common.AppError{nil, "Name is required. Min 2 chars", http.StatusBadRequest};
	}

	if (!govalidator.IsEmail(email)) {
		return 0, &common.AppError{nil, "Email is required", http.StatusBadRequest};
	}

	stmt, err := u.db.Prepare("insert into users(name, email) values(?, ?)")
	if err != nil {
		u.logger.Println(err.Error());
		return 0, &common.AppError{err, "DB Error", http.StatusInternalServerError};
	}

	//res, err := stmt.Exec(sql.NamedArg{Name: "name", Value: name}, sql.NamedArg{Name: "email", Value: email});
	res, err := stmt.Exec(name, email);

	if err != nil {
		u.logger.Println(err.Error());
		return 0, &common.AppError{err, "DB Error", http.StatusInternalServerError};
	}

	lastId, err := res.LastInsertId()
	if err != nil {
		return 0, &common.AppError{err, "DB Error", http.StatusInternalServerError};
	}

	return lastId, nil
}

func (u *user) FindByName(name string) ([]*User, *common.AppError) {
	rows, err := u.db.Query("select id, name from users where name like ?", "%" + name + "%")
	if err != nil {
		u.logger.Println(err.Error())
		return nil, &common.AppError{err, "DB Server error", http.StatusInternalServerError}
	}

	defer rows.Close()

	var users []*User
	for rows.Next() {
		user := new(User)
		err := rows.Scan(&user.Id, &user.Name)
		if err != nil {
			u.logger.Println(err.Error())
			return nil, &common.AppError{err, "DB Server error", http.StatusInternalServerError}
		}

		users = append(users, user)
	}

	return users, nil
}
