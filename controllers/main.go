package controllers
import (
	"encoding/json"
	"net/http"
	"bitbucket.org/xappz/golang-mysql/models"
	"bitbucket.org/xappz/golang-mysql/deps"
	"bitbucket.org/xappz/golang-mysql/constants"
	"bitbucket.org/xappz/golang-mysql/common"
)
//Refer: https://blog.golang.org/error-handling-and-go

type ctrlr struct {
	handler func(*http.Request) (interface{}, *common.AppError)
	viewer func(http.ResponseWriter, interface{})
}

type view struct {
	Status string `json:"status"`
	Data interface{} `json:"data,omitempty"`
	Msg string `json:"msg,omitempty"`
	Code int `json:"code,omitempty"`
}

func (c ctrlr) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	data, e := c.handler(r)
	if e != nil {
		sendError(w, e)
		return
	}

	if (c.viewer != nil) {
		//if the controller has defined its own viewer then use it, 
		//otherwise stick to default implementation
		c.viewer(w, data)
		return
	}

	sendSuccess(w, data);
}

func sendError(w http.ResponseWriter, e *common.AppError) {
	out := view{Status: "error", Msg: e.Message, Code: e.Code}
	js, err := json.Marshal(out)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(js)
}

func sendSuccess(w http.ResponseWriter, data interface{}) {
	out := view{Status: "ok", Data: data}
	js, err := json.Marshal(out)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(js)
}

func Create(c deps.Container, name string) http.Handler {
	switch (name) {
	case constants.UsrCtrlr:
		logger := c.Logger(constants.UsrCtrlr)
		return UsrCtrlr(logger, models.NewUser(c.Db, logger))
	}

	return nil
}
