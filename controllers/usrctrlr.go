package controllers
import (
	"log"
	//"encoding/json"
	"bitbucket.org/xappz/golang-mysql/models"
	"bitbucket.org/xappz/golang-mysql/common"
	"strconv"
	"github.com/gorilla/mux"
	"net/http"
)

type Out struct {
	Users interface{} `json:"users"`
}

func UsrCtrlr(logger *log.Logger, user models.Usr) http.Handler {
	return ctrlr{
		handler: func(r *http.Request) (interface{}, *common.AppError) {
			switch (r.Method) {
			case http.MethodGet:
				return handleGet(r, logger, user)

			case http.MethodPost:
				return handlePost(r, logger, user)

			default:
				return nil, &common.AppError{nil, "Not found", http.StatusNotFound}
			}
		}}
}

func handlePost(r *http.Request, logger *log.Logger, user models.Usr) (interface{}, *common.AppError) {
	logger.Println(r.FormValue("name"));
	name := r.FormValue("name");
	email := r.FormValue("email");
	id, err := user.Save(name, email);
	if (err != nil) {
		return nil, err
	}

	return id, nil
}

func handleGet(r *http.Request, logger *log.Logger, m models.Usr) (interface{}, *common.AppError) {
	v := r.URL.Query()
	if (v.Get("name") != "") {
		users, aerr := m.FindByName(v.Get("name"))
		if (aerr != nil) {
			return nil, aerr
		}

		return Out{Users: users}, nil
	}

	vars := mux.Vars(r)
	id, err := strconv.Atoi(vars["id"])

	if (err != nil) {
		return nil, &common.AppError{err, "Invalid input", http.StatusBadRequest}
	}

	user, aerr := m.FindById(id)
	if (aerr != nil) {
		logger.Println("FindById error")
		return nil, aerr
	}

	var users = []*models.User {user}

	return Out{Users: users}, nil
}
