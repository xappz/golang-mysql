package main

import (
	"bitbucket.org/xappz/golang-mysql/constants"
	"bitbucket.org/xappz/golang-mysql/controllers"
	"bitbucket.org/xappz/golang-mysql/deps"
	"bitbucket.org/xappz/golang-mysql/renderer"
	"bitbucket.org/xappz/golang-mysql/settings"
	"github.com/gorilla/mux"
	"log"
	"net/http"
)

func main() {
	settings.Create()
	container := deps.Create()
	defer container.Destroy()

	r := mux.NewRouter()
	r.Handle("/api/users/{id}", controllers.Create(container, constants.UsrCtrlr)).Methods("GET")
	r.Handle("/api/users", controllers.Create(container, constants.UsrCtrlr)).Methods("GET", "POST")
	r.Handle("/{page}", renderer.Create(container)).Methods("GET")
	r.PathPrefix("/").Handler(http.StripPrefix("/", http.FileServer(http.Dir("assets/"))))

	log.Println("Listening at port 3000")
	http.ListenAndServe(":3000", r)
}
