package common
type AppError struct {
    Err error
    Message string
    Code    int
}

func (e AppError) Error() string {
	return e.Message
}
